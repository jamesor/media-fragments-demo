import React, { Component } from 'react';
import Header from '../components/Header';
import Footer from '../components/Footer';
import baseStyles from '../styles/base.scss';
import s from './index.scss';

const App = (props) => {
  return (
    <div id="top">
      <Header />
      {props.children}
      <Footer />
    </div>
  );
}

export default App;