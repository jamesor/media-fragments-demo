import React from 'react';
import s from './index.scss';

const Error404 = (props) => {
  return (
    <main className={s.main}>
      <h1>404</h1>
    </main>
  );
}

export default Error404;