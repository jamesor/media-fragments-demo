import React from 'react';
import { Link } from 'react-router'
import VideoList from 'core/video/VideoList';
import s from './videos.scss';

const renderList = () => {
  return VideoList.map((v) => {
    const href = `/video/${v._id}`;
    return (
      <li key={v._id}>
        <Link to={href}>{v.name}</Link>
      </li>
    );
  });
};

const Videos = (props) => {
  return (
    <main className={s.root}>
      <h1>Choose a video to edit</h1>
      <ul>
        {renderList()}
      </ul>
    </main>
  );
}

export default Videos;