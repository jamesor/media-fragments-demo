import React, { Component, PropTypes } from 'react';
import VideoPlayer from 'views/components/VideoPlayer';
import VideoPlayerControls from 'views/components/VideoPlayerControls';
import s from './video.scss';

const Video = (props) => {
  return (
    <main className={s.root}>
      <VideoPlayer videoId={props.params.videoId} />
      <VideoPlayerControls videoId={props.params.videoId} />
    </main>
  );
}

Video.propTypes = {
  videoId: PropTypes.string
};

export default Video;