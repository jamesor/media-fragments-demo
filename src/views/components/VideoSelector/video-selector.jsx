import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { changeVideo } from 'core/video/actions';
import { push } from 'react-router-redux';
import VideoList from 'core/video/VideoList';
import s from './video-selector.scss';

const renderOptions = (videos) => {
  return videos.map((item) => <option {...item}>{item.name}</option>);
};

const VideoSelector = ({videos, videoId, onSelect}) => {
  return (
    <section className={s.root}>
      <label htmlFor="videoSelector" className={s.title}>Choose a video:</label>
      <select
        id="videoSelectorAA"
        className={s.list}
        value={videoId}
        onChange={(e) => onSelect(e.target.value)}
      >
        {renderOptions(videos)}
      </select>
    </section>
  );
};

VideoSelector.propTypes = {
  videoId: PropTypes.string,
  videos: PropTypes.arrayOf(PropTypes.shape({
    key: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  }))
};


//-------------------------------------
// connect
//

const mapStateToProps = (state, ownProps) => {
  let videoId = ownProps.videoId;
  let videos = VideoList.map(v => ({
    key: v._id,
    name: v.name,
    value: v._id,
  }));
  return { videoId, videos };
};

const mapDispatchToProps = (dispatch) => ({
  onSelect(videoId) {
    dispatch(push(`/video/${videoId}`));
  }
});

export default  connect(
  mapStateToProps,
  mapDispatchToProps,
)(VideoSelector);
