import React, { Component, PropTypes } from 'react';
import IconButton from '../IconButton';
import s from './video-playlist.scss';
import classNames from 'classnames';

class VideoPlaylistItem extends Component {
  constructor(props) {
    super(props);

    this.state = { clip: Object.assign({}, props.clip), editmode: false };
    this.onEditClick = this.onEditClick.bind(this);
    this.onSaveClick = this.onSaveClick.bind(this);
    this.onDeleteClick = this.onDeleteClick.bind(this);
    this.onValueChange = this.onValueChange.bind(this);
  }

  onEditClick(e) {
    e.stopPropagation();
    this.setState({ editmode: true });
  }

  onSaveClick(e) {
    e.stopPropagation();
    this.props.onSave(this.state.clip);
    this.setState({ editmode: false });
  }

  onDeleteClick(e) {
    e.stopPropagation();
    if (confirm('Are you certain you want to delete this clip?')) {
      this.props.onDelete(this.state.clip.key);
    }
  }

  onCancelClick(e) {
    e.stopPropagation();
    this.setState({ clip: this.props.clip, editmode: false });
  }

  onValueChange(e) {
    let name = e.target.name;
    let value = (isNaN(e.target.value)) ? e.target.value : Number(e.target.value);
    let clip = Object.assign({}, this.state.clip, {[name]: value});
    this.setState({ clip });
  }

  render() {
    let {clip, onPlay, selected} = this.props;
    let isEditMode = this.state.editmode;
    let isAddItem = this.state.clip.key == -1;

    const cn = classNames({
      [s.listitem]: true,
      [s.selected]: selected,
      [s.editmode]: isEditMode
    });

    if (isEditMode || isAddItem) {

      let cancelButton;
      if (!isAddItem) {
        cancelButton = <IconButton onClick={(e) => this.onCancelClick(e)} label="cancel" icon="ban" />
      }

      return (
        <li className={cn}>
          <div>
            <input
              type="text"
              name="name"
              value={this.state.clip.name}
              onChange={this.onValueChange}
              placeholder="Clip name" />
            <div className={s.range}>
              <label htmlFor="start">From</label>
              <input
                id="start"
                className={s.num}
                type="number"
                name="start"
                min="0"
                max={this.state.clip.end}
                value={this.state.clip.start}
                onChange={this.onValueChange} />
              <label htmlFor="end">to</label>
              <input
                id="end"
                className={s.num}
                type="number"
                name="end"
                min={this.state.clip.start}
                value={this.state.clip.end}
                onChange={this.onValueChange} />
            </div>
          </div>
          <div className={s.actions}>
            {cancelButton}
            <IconButton onClick={(e) => this.onSaveClick(e)} label="save" icon="floppy-o" />
          </div>
        </li>
      );
    }

    return (
      <li className={cn} onClick={() => onPlay(clip)} role="button">
        <div>
          <h3 className={s.name}>
            {clip.name}
          </h3>
          <div className={s.range}>From {clip.start} to {clip.end}</div>
        </div>
        <div className={s.actions}>
          <IconButton onClick={(e) => this.onEditClick(e)} label="Edit" icon="pencil" />
          <IconButton onClick={(e) => this.onDeleteClick(e)} label="Delete" icon="trash" />
        </div>
      </li>
    );
  }
}

VideoPlaylistItem.propTypes = {
  clip: PropTypes.shape({
    key: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    start: PropTypes.number.isRequired,
    end: PropTypes.number.isRequired
  }).isRequired,
  onPlay: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  selected: PropTypes.bool.isRequired
};

export default VideoPlaylistItem;