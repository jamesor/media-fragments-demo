import React, { Component, PropTypes } from 'react';
import VideoPlaylistItem from './video-playlist-item.jsx';
import { connect } from 'react-redux';
import VideoList from 'core/video/VideoList';
import { playClip, createClip, updateClip, deleteClip } from 'core/video/actions';

import s from './video-playlist.scss';

const renderClips = ({clips, onPlay, onSave, onDelete, selectedClipId}) => {
  return clips.map(clip => {
    return (
      <VideoPlaylistItem
        key={clip.key}
        clip={clip}
        onPlay={onPlay}
        onSave={onSave}
        onDelete={onDelete}
        selected={selectedClipId === clip.key}
      />
    );
  });
};

const playVideo = (props) => {
  props.onPlay();
}

const VideoPlaylist = (props) => {
  return (
    <section className={s.root}>
      <h2 className={s.title}>You are watching:</h2>
      <div className={s.orig} onClick={() => playVideo(props)}>
        <h3>{props.original.name}</h3>
      </div>
      <ul className={s.list}>
        {renderClips(props)}
      </ul>
    </section>
  );
};

VideoPlaylist.propTypes = {
  original: PropTypes.shape({
    key: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired
  }),
  selectedClipId: PropTypes.number,
  clips: PropTypes.array,
  onPlay: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired
};


//-------------------------------------
// connect
//

const ADD_NEW_ITEM_KEY = -1;

var key = 0;

const mapStateToProps = ({video, filter}, ownProps) => {
  let { videoId, selectedClipId, clips } = video;
  let v = VideoList.find(v => v._id === ownProps.videoId);

  if (typeof v === 'undefined') {
    return [];
  }

  let original = {
    key: key++,
    name: v.name
  };

  const newClip = {
    key: ADD_NEW_ITEM_KEY,
    name: 'Add clip',
    start: 0,
    end: 0
  };

  if (filter.term) {
    clips = clips.filter(c => {
      return c.name.toLowerCase().indexOf(filter.term.toLowerCase()) >= 0;
    });
  }

  clips = [...clips, newClip];

  return { original, selectedClipId, clips };
};

const mapDispatchToProps = (dispatch) => ({
  onPlay(clip) {
    dispatch(playClip(clip));
  },
  onSave(clip) {
    let isNewClip = (clip.key == ADD_NEW_ITEM_KEY);

    if (isNewClip) {
      dispatch(createClip(clip));
    } else {
      dispatch(updateClip(clip));
    }
  },
  onDelete(id) {
    dispatch(deleteClip(id));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VideoPlaylist);
