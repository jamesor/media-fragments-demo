import React from 'react';
import VideoSelector from '../VideoSelector';
import VideoFilter from '../VideoFilter';
import VideoPlaylist from '../VideoPlaylist';
import s from './video-player-controls.scss';

const VideoPlayerControls = (props) => {
  return (
    <div className={s.root}>
      <VideoSelector videoId={props.videoId} />
      <VideoFilter />
      <VideoPlaylist videoId={props.videoId} />
    </div>
  );
}

export default VideoPlayerControls;