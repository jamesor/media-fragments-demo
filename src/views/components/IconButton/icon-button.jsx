import React, { PropTypes } from 'react';
import FontAwesome from 'react-fontawesome';
import s from './icon-button.scss';

const IconButton = ({label, icon, onClick}) => {
  return (
    <button onClick={onClick} className={s.root}>
      <FontAwesome name={icon} />
      {label}
    </button>
  );
};

IconButton.propTypes = {
  icon: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
};

IconButton.defaultProps = {
  icon: 'star-o',
  label: 'Button',
};

export default IconButton;