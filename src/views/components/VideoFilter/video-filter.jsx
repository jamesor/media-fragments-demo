import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { filterClips } from '../../../core/video/actions';
import s from './video-filter.scss';

class VideoFilter extends Component {
  constructor(props) {
    super(props);

    this.state = { term: '' };
    this.onChange = this.onChange.bind(this);
  }

  onChange(e) {
    this.setState({term: e.target.value});
    this.props.onChange(e.target.value);
  }

  render() {
    return (
      <section className={s.root}>
        <label htmlFor="videoFilter" className={s.title}>Filter:</label>
        <input
          id="videoFilter"
          className={s.filter}
          value={this.state.term}
          onChange={this.onChange}
        />
      </section>
    );
  }
};

VideoFilter.propTypes = {
  onChange: PropTypes.func.isRequired
};


//-------------------------------------
// connect
//

const mapDispatchToProps = (dispatch) => ({
  onChange(term) {
    dispatch(filterClips(term));
  }
});

export default connect(
  null,
  mapDispatchToProps
)(VideoFilter);
