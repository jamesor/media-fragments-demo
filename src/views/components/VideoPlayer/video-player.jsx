import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import VideoList from 'core/video/VideoList';
import s from './video-player.scss';

const renderSources = (sources) => {
  return sources.map((src) => {
    return <source key={src.key} src={src.url} />
  });
};

class VideoPlayer extends Component {
  componentDidUpdate() {
    ReactDOM.findDOMNode(this.refs.video)
            .addEventListener('ended', () => this.props.onClipEnd());
  }

  render() {
    return (
      <div className={s.root}>
        <video key={this.props.id} ref="video" className={s.video} controls autoPlay>
          { renderSources(this.props.sources) }
        </video>
      </div>
    );
  }
}

VideoPlayer.propTypes = {
  id: React.PropTypes.string,
  sources: PropTypes.arrayOf(
    React.PropTypes.shape({
      url: React.PropTypes.string,
      key: React.PropTypes.string
    })
  )
};


//-------------------------------------
// connect
//

const mapStateToProps = ({video}, ownProps) => {
  let { videoId, selectedClipId, clips } = video;
  let v = VideoList.find(v => v._id === ownProps.videoId);

  if (typeof v === 'undefined') {
    return [];
  }

  let url, target;
  let clip = clips.find(c => c.key === selectedClipId);
  let sources = v.sources.map(src => {
    target = (clip && clip.start) ? `#t=${clip.start},${clip.end}` : '';
    url = `${src}${target}`;
    return ({
      key: url,
      url
    });
  });

  return { id: url, sources };
};

const mapDispatchToProps = (dispatch) => ({
  onClipEnd(clip) {
    console.log('clip ended');
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VideoPlayer);
