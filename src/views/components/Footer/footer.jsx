import React from 'react';
import s from './footer.scss';

const Footer = () => {

  const year = (new Date()).getFullYear();

  return (
    <div className={s.root}>
      &copy; {year} James O'Reilly
    </div>
  );
}

export default Footer;