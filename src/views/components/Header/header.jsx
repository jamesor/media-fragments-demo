import React from 'react';
import s from './Header.scss';

const Header = (props) => {
  return (
    <header className={s.root}>
      <h1 className={s.title}>Media Fragments Demo</h1>
    </header>
  );
}

export default Header;