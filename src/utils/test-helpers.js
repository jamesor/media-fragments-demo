const localStorageMock = (function() {
  let store = {};
  return {
    removeItem(k) { delete store[k]; return true; },
    setItem(k, v=[]) { if (k) { store[k]=v; return true; } },
    getItem(k) { return store[k]; },
    clear() { store = {} },
  }
})();
Object.defineProperty(window, 'localStorage', { value: localStorageMock });