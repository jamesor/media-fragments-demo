import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute } from 'react-router'
import injectTapEventPlugin from 'react-tap-event-plugin';
import { store, history } from 'core/store';

import App from 'views/App';
import Error404 from 'views/App/404'
import Video from 'views/pages/Video';
import Videos from 'views/pages/Videos';

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

// Wait until the browser is ready to render the app (avoids glitches)
window.requestAnimationFrame(function () {
  render((
    <Provider store={store}>
      <Router history={history}>
        <Route path="/" component={App}>
          <IndexRoute component={Videos} />
          <Route path="video" component={Videos} />
          <Route path="video/:videoId" component={Video} />
          <Route path="*" component={Error404} />
        </Route>
      </Router>
    </Provider>
  ), document.getElementById('app'));
});