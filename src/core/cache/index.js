const getId = (id) => `v1-${id}`;

export const hasValue = (id) => (typeof localStorage.getItem(getId(id)) !== 'undefined');
export const delValue = (id) => localStorage.removeItem(getId(id));

export const setValue = (id, val=[]) => {
  if (id) {
    localStorage.setItem(getId(id), JSON.stringify(val));
    return true;
  }
};

export const getValue = (id) => {
  const val = localStorage.getItem(getId(id));
  return (val) ? JSON.parse(val) : false;
}

// SEED THE CACHE FOR DEMO PURPOSES. Eventually this will come out.
if (localStorage.length === 0) {
  setValue('001', [
    { key: 0, start:  7, end: 20, name: `We're walking, we're walking...` },
    { key: 1, start: 28, end: 32, name: `Smackdown` },
    { key: 2, start: 67, end: 77, name: `Run Forest. Run.` }
  ]);
}