import expect from 'expect';
import * as cache from './';

describe('cache', () => {

  it('should return undefined when ID is bogus', () => {
    expect(
      cache.getValue('foo')
    ).toNotExist()
  });

  it('should save data and retrieve it', () => {
    let val = Math.random();
    cache.setValue('foo', val);
    expect(
      cache.getValue('foo')
    ).toEqual(val)
  });

  it('should know if it has data', () => {
    expect(
      cache.hasValue('foo')
    ).toBeTruthy()
  });

  it('should delete data', () => {
    expect(
      cache.delValue('foo')
    ).toBeTruthy()
  });

});