import * as types from './action-types';

var initialState = {
  term: ''
};

const filter = (state = initialState, action) => {

  switch (action.type) {
    case types.CLIPS_FILTER:
      return { term: action.term };

    default:
      return state;
  }

};

export default filter;