import {
  VIDEO_CHANGE,
  CLIP_CREATE,
  CLIP_READ,
  CLIP_UPDATE,
  CLIP_DELETE,
  CLIP_PLAY,
  CLIPS_FILTER,
} from './action-types.js';

export const changeVideo = id => ({
  type: VIDEO_CHANGE,
  payload: id,
});

export const createClip = (clip) => ({
  type: CLIP_CREATE,
  payload: clip
});

export const readClip = (key) => ({
  type: CLIP_READ,
  payload: key
});

export const updateClip = (clip) => ({
  type: CLIP_UPDATE,
  payload: clip
});

export const deleteClip = (key) => ({
  type: CLIP_DELETE,
  payload: key
});

export const playClip = (clip) => ({
  type: CLIP_PLAY,
  payload: clip
});

export const filterClips = (term) => ({
  type: CLIPS_FILTER,
  payload: term
});