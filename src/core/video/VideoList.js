export default [
  {
    _id: '001',
    name: 'Star Wars: Rogue One',
    sources: [
      'http://movietrailers.apple.com/movies/lucasfilm/rogueoneastarwarsstory/rogueone-tsr1_i320.m4v'
      //'/media/rogueone-tsr1_i320.m4v'
    ]
  },
  {
    _id: '002',
    name: 'Star Wars: The Force Awakens',
    sources: [
      'http://movietrailers.apple.com/movies/lucasfilm/starwarstheforceawakens/starwars-tvspot_i320.m4v'
      //'/media/starwars-tvspot_i320.m4v'
    ]
  }
];
