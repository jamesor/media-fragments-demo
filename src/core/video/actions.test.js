import expect from 'expect';
import * as actions from './actions';
import {
  VIDEO_CHANGE,
  CLIP_CREATE,
  CLIP_READ,
  CLIP_UPDATE,
  CLIP_DELETE,
  CLIP_PLAY,
  CLIPS_FILTER,
} from './action-types.js';

const clip = {
  key: 1,
  name: 'The Clip',
  start: 15,
  end: 30
}

describe('VideoActions', () => {

  it('should change videos', () => {
    const expectedAction = {
      type: VIDEO_CHANGE,
      payload: '002',
    }
    expect(actions.changeVideo('002')).toEqual(expectedAction)
  });

  it('should create an action to add a clip', () => {
    const expectedAction = {
      type: CLIP_CREATE,
      payload: clip,
    }
    expect(actions.createClip(clip)).toEqual(expectedAction)
  })

  it('should create an action to read a clip', () => {
    const key = 100;
    const expectedAction = {
      type: CLIP_READ,
      payload: key,
    }
    expect(actions.readClip(key)).toEqual(expectedAction)
  })

  it('should create an action to update a clip', () => {
    const expectedAction = {
      type: CLIP_UPDATE,
      payload: clip
    }
    expect(actions.updateClip(clip)).toEqual(expectedAction)
  })

  it('should create an action to delete a clip', () => {
    const key = 100;
    const expectedAction = {
      type: CLIP_DELETE,
      payload: key,
    }
    expect(actions.deleteClip(key)).toEqual(expectedAction)
  })

  it('should create an action to play a clip', () => {
    const expectedAction = {
      type: CLIP_PLAY,
      payload: clip,
    }
    expect(actions.playClip(clip)).toEqual(expectedAction)
  })

  it('should create an action to filter clips', () => {
    const expectedAction = {
      type: CLIPS_FILTER,
      payload: 'term',
    };
    expect(actions.filterClips('term')).toEqual(expectedAction)
  })
});