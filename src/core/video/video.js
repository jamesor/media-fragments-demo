import * as actions from './actions';
import * as types from './action-types';
import * as cache from '../cache';
import { LOCATION_CHANGE } from 'react-router-redux';
import VideoList from './VideoList';

let key = 0;
let oldVideoId;

const initialState = {
  selectedClipId: null,
  clips: []
};

const putInCache = (id, val) => {
  cache.setValue(id, val);
};

const getFromCache = (id) => {
  oldVideoId = id;
  return {
    selectedClipId: null,
    clips: (cache.getValue(id) || []),
  };
};

const video = (state = initialState, action) => {
  let clips, newClip;

  const clipSortFn = (a, b) => (a.start - b.start);

  switch (action.type) {
    case types.CLIP_PLAY:
      return Object.assign({}, state, {
        selectedClipId: (action.clip) ? action.clip.key : null
      });

    case types.CLIP_CREATE:
      newClip = Object.assign({}, action.clip, { key: key++ });
      clips = [...state.clips, newClip].sort(clipSortFn);
      putInCache(oldVideoId, clips);
      return Object.assign({}, state, { clips });

    case types.CLIP_UPDATE:
      clips = state.clips.map((clip) => {
        return (clip.key === action.clip.key)
          ? Object.assign({}, clip, action.clip)
          : clip;
      }).sort(clipSortFn);
      putInCache(oldVideoId, clips);
      return Object.assign({}, state, { clips });

    case types.CLIP_DELETE:
      clips = state.clips.filter((clip) => (clip.key !== action.key)).sort(clipSortFn);
      putInCache(oldVideoId, clips);
      return Object.assign({}, state, { clips });

    case LOCATION_CHANGE:
      const isNotVideoRoute = ((/^\/video\//.test(action.payload.pathname)) === false);
      if (isNotVideoRoute) {
        return state;
      }

      const newVideoId = action.payload.pathname.replace('/video/', '');
      const isSameVideo = (newVideoId === oldVideoId);
      if (isSameVideo) {
        return state;
      }

      // If this is a bad route, we have no way of handling it so
      // send them to the homepage instead with a hard reset.
      const found = (VideoList.find((v) => (v._id === newVideoId)));
      const isInvalidVideoId = (typeof found === 'undefined');
      if (isInvalidVideoId) {
        document.location.href = "/";
      }

      putInCache(oldVideoId, state.clips);
      return getFromCache(newVideoId);

    default:
      return state;
  }
};

export default video;
