import expect from 'expect';
import reducer from './video';
import {
  CLIP_CREATE,
  CLIP_UPDATE,
  CLIP_DELETE,
  CLIPS_FILTER,
} from './action-types.js';

describe('video reducer', () => {
  const state = {
    selectedClipId: null,
    clips: [
      {
        key: 0,
        name: 'A starter clip',
        start: 10,
        end: 20
      }
    ]
  };

  it('should handle CREATE_CLIP', () => {
    let newState = reducer(state, {
      type: CLIP_CREATE,
      clip: {
        name: 'My first added clip',
        start: 20,
        end: 30
      }
    });

    expect(
      newState.clips.find(c => c.name === 'My first added clip')
    ).toExist()
  })

  it('should handle UPDATE_CLIP', () => {
    expect(
      reducer(state, {
        type: CLIP_UPDATE,
        clip: {
          key: 0,
          name: 'Changed clip title',
          start: 10,
          end: 50
        }
      })
    ).toEqual(
      {
        selectedClipId: null,
        clips: [
          {
            key: 0,
            name: 'Changed clip title',
            start: 10,
            end: 50
          }
        ]
      }
    )
  })

  it('should handle DELETE_CLIP', () => {
    expect(
      reducer(state, {
        type: CLIP_DELETE,
        key: 0
      })
    ).toEqual(
      {
        selectedClipId: null,
        clips: []
      }
    )
  })

  it('should handle CLIPS_FILTER', () => {
    expect(
      reducer(state, {
        type: CLIPS_FILTER,
        filter: 'term'
      })
    ).toEqual(
      {
        selectedClipId: null,
        clips: [
          {
            key: 0,
            name: 'A starter clip',
            start: 10,
            end: 20
          }
        ]
      }
    )
  })
})