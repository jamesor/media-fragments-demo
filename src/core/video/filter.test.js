import expect from 'expect';
import reducer from './filter';
import * as types from './action-types';

describe('filter reducer', () => {

  it('should handle CLIPS_FILTER', () => {
    expect(
      reducer([], {
        type: types.CLIPS_FILTER,
        term: 'term'
      })
    ).toEqual(
      {
        term: 'term'
      }
    )
  });

});