export const CLIP_CREATE  = 'CLIP_CREATE';
export const CLIP_READ    = 'CLIP_READ';
export const CLIP_UPDATE  = 'CLIP_UPDATE';
export const CLIP_DELETE  = 'CLIP_DELETE';
export const CLIP_PLAY    = 'CLIP_PLAY';

export const CLIPS_FILTER  = 'CLIPS_FILTER';

export const VIDEO_CHANGE = 'VIDEO_CHANGE';