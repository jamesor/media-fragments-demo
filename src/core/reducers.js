import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import video from './video/video';
import filter from './video/filter';

const reducers = combineReducers({
  video,
  filter,
  routing: routerReducer
});

export default reducers;