import { browserHistory } from 'react-router'
import { routerMiddleware, syncHistoryWithStore } from 'react-router-redux';
import { createStore, applyMiddleware } from 'redux';
import rootReducer from './reducers';

const middleware = routerMiddleware(browserHistory);

export const store = createStore(rootReducer, applyMiddleware(middleware));
export const history = syncHistoryWithStore(browserHistory, store);
