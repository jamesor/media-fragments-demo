# Media Fragments Demo

This is a demonstration of HTML5 video element [media fragments](https://www.w3.org/TR/media-frags/) features.

## Table of Contents

+ [Prolog](#markdown-header-prolog)
+ [Dowloading the Demo](#markdown-header-downloading-the-demo)
+ [Running the Demo](#markdown-header-running-the-demo)
+ [Questions and Answers](#markdown-header-questions-and-answers)

## Prolog

The demo was created using the following technologies:

+ [ReactJS](https://facebook.github.io/react/) for UI component development
+ [Redux](http://redux.js.org/) for centralized state management
+ [Webpack](https://webpack.github.io/) for bundling code and resources for optimized delivery
+ [CSS Modules](https://github.com/gajus/react-css-modules) for object oriented styles
+ [Babel](https://babeljs.io/) for ES2015 syntax today
+ [Mocha](https://mochajs.org/) and [Expect](https://www.npmjs.com/package/expect) for unit testing

## Downloading the Demo

Clone the demo to your computer

```bash
git clone git@bitbucket.org:jamesor/media-fragments-demo.git
```

Make sure you have Node.js installed. I recommend using [nvm](https://github.com/creationix/nvm) to manage your Node.js versions. Installation is outside the scope of this document.

Change directories to the project repo and install local dependencies required to build and run the application.

```bash
cd media-fragments-demo
npm install
```

## Running the Demo

Included are a few npm scripts that you can run for different reasons and are all run from the command line in the project's directory.

+ `npm start` This is will clean, build and run the application with development presets. It can be accessed at http://127.0.0.1:8080
+ `npm run build` This will clean and build the application to the /dist folder highly optimized and ready for deployent.
+ `npm run test` This will run all tests and output results to the terminal.

## Using the Demo

The demo is pretty simple to use and doesn't require much explaination. Here are some of the features to explore.

+ **Change the video** using the Choose a Video dropdown. Your clips will be saved and restored.
+ **Filter the video** clips using the Filter textfield.
+ **Play a video clip** by clicking it.
+ **Edit a video clip** by hovering the clip list item and clicking the Edit button. Edit the fields and click the Save or Cancel button.
+ **Delete a video clip** by hovering the clip list item and clicking the Delete button. Agree to the prompt.
+ **Add a video clip** by using the form at the bottom of the clip list and then click the Save button.

## Questions and Answers

### Isn't that setup a tad overkill for a video demo?

Yes.

However, I also wanted to set up the project as if it were going to grow into a much larger application. To do that, I wanted to included the right amount of project organization and tooling to meet that goal.

### Okay, so what tooling is there?

+ Two build processes. [Webpack dev server](https://webpack.github.io/docs/webpack-dev-server.html) is used during development with live updates and a more optimized build is created for production.
+ CSS Modules so that each component could have simplified css style naming without the fear of conflict because [BEM](http://getbem.com/) style naming is applied automatically in the build process. A human readable format for dev builds and a more efficient format for production builds.
+ In production builds, CSS and JavaScript is optimized and sourcemaps are generated for production debugging.
+ Babel is used so that ReactJS can be developed with ES2015 features and JSX.
+ In production builds, images smaller than 4k are inlined as base64.
+ Tests are mixed in with the source rather than a separate test folder. I find it easier to find the tests, remember that I should be working on them, and easier to link in dependencies.