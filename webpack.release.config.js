var ExtractTextPlugin = require('extract-text-webpack-plugin');
var webpack           = require('webpack');
var atImport          = require('postcss-import');
var customProperties  = require('postcss-custom-properties');
var autoprefixer      = require('autoprefixer');
var path              = require('path');
var CleanPlugin       = require('clean-webpack-plugin');

var distPath  = path.resolve(__dirname, 'dist');
var srcPath   = path.resolve(__dirname, 'src');
var entryPath = path.resolve(srcPath, 'index.jsx');

module.exports = {
  entry: [
    entryPath
  ],

  devtool: 'cheap-module-source-map',

  module: {
    loaders:[
      { test: /\.js[x]?$/, exclude: /node_modules/, loader: 'babel' },
      { test: /\.scss$/, loader: ExtractTextPlugin.extract('style', ['css?localIdentName=[hash:base64]&modules', 'postcss', 'sass']) },
      { test: /\.(png|jpg)$/, loader: 'file?limit=4096&name=images/[name].[ext]' },
      { test: /\.woff$/, loader: 'file?name=fonts/[name].[ext]' },
      { test: /\.html$/, loader: 'file?name=[name].[ext]' }
    ]
  },

  output: {
    path: distPath,
    publicPath: '/',
    filename: 'bundle.js'
  },

  plugins: [
    new CleanPlugin(['dist'], {
      root: path.resolve(__dirname),
      verbose: true,
      dry: false
    }),
    new webpack.DefinePlugin({
      __DEV__: JSON.stringify(JSON.parse('true')),
      __RELEASE__: JSON.stringify(JSON.parse('false'))
    }),
    new ExtractTextPlugin('bundle.css'),
    //new webpack.optimize.CommonsChunkPlugin('common.js'),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin(),
    new webpack.optimize.AggressiveMergingPlugin()
  ],

  postcss: [
    atImport({
      path: ['node_modules', srcPath]
    }),
    autoprefixer({
      browsers: ['last 2 versions']
    }),
    customProperties()
  ],

  resolve: {
    extensions: ['', '.js', '.jsx'],
    root: [srcPath],
    modulesDirectories: ['node_modules', 'src'],
  }
};
